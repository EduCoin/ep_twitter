<?php 
$currentPage = "home";
if(isset($_GET['page'])) {

	switch ($_GET['page']) {
		
		case 'home':
			$currentPage = "home";

			include "views/home.php";
			break;
		
		case 'contacts':
			$currentPage = "contacts";

			include "views/contacts.php";
			break;

		case 'about':
			$currentPage = "about";
			include "views/about.php";
			break;

		case 'account':
			// @todo if !isset(user)
			$currentPage = "account";
			include "views/account.php";
			break;

		case 'upload-photo':
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				
				include "controllers/controllerAuth.php";
				uploadPhoto($_POST);

			} else {

				header('Location: /account');
			
			}

			break;

		case 'upload-photo2':
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				
				include "controllers/controllerAuth.php";
				uploadPhoto2($_POST);

			} else {

				header('Location: /account');
			
			}

			break;

		case 'login':
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				
				include "controllers/controllerAuth.php";
				login($_POST);

			} else {
				$currentPage = "login";
				include "views/auth/login.php";
			}

			break;

		case 'logout':
			unset($_SESSION['user']);
			header('Location: /home');

			break;

		case 'register':
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$currentPage = "register";
				
				include "controllers/controllerAuth.php";
				registerUser($_POST);
			} else {
				$currentPage = "register";
				include "views/auth/register.php";

			}
			
			break;

		case 'validate':
			die($_GET['key']);
			
			break;

		default:
			include "views/home.php";
			break;
	}

} else {
	include "views/home.php";
}

?>