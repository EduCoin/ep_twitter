<?php

include 'model.php';

$tableName = "users";

function addUser($username, $email, $password) {

	global $conn;

	$username = $conn->real_escape_string( trim( htmlspecialchars( $username ) ) );
	$email = $conn->real_escape_string( trim( htmlspecialchars( $email ) ) );
	$password = $conn->real_escape_string( trim( htmlspecialchars( $password ) ) );

	if($username == '') {
		return [
			'status' => 'error',
			'field' => 'username',
			'message' => 'Enter your name',
		];
	}

	$validate = generateValidateKey();

	$sql = "Insert into users (username, email, password, validate_key) values ('$username', '$email' , '$password', '$validate')";

	if ($conn->query($sql) == TRUE) {

	    return [
	    	'code' => 100,
	    	'status' => true, 
	    	'message' => 'insert',
	    	'validate_key' => $validate
    	];
	
	} else {
    
    	return [
	    	'code' => 102,
	    	'status' => false, 
	    	'message' => "Error: " . $sql . "<br>" . $conn->error
    	];
	
	}
}

function findBy($arr = array()) {

	// ['email' => '', 'pass', 'username']
	global $conn;

	$email = $conn->real_escape_string( trim( htmlspecialchars( $arr['email'] ) ) );
	$password = $conn->real_escape_string( trim( htmlspecialchars( $arr['password'] ) ) );

    //Здесь валидация пароля и логина
    $result = $conn->query("SELECT * FROM users WHERE email='$email' && password='$password'");
    
    if($result->num_rows > 0){
        return $result->fetch_assoc();
    }

    return 0;
}

function updateUser($userId, $key, $value) {
	global $conn;

	$sql = "Update users SET $key='$value' WHERE id='$userId'";

	if ($conn->query($sql) == TRUE) {

	    return [
	    	'code' => 100,
	    	'status' => true, 
	    	'message' => 'update'
    	];
	
	} else {
    
    	return [
	    	'code' => 102,
	    	'status' => false, 
	    	'message' => "Error: " . $sql . "<br>" . $conn->error
    	];
	
	}
}

function generateValidateKey() {
	return 'qwertyusfsfsd4536yfd4tvdd34';
}