<?php 
include 'views/partials/header.php';
?>

<div id="wrapper" class="bg-page register-bg">
    <?php
    $error = null;
    if(isset($_SESSION['register_error'])) {
    	$error = $_SESSION['register_error'];
    }
    ?>
    <form id="register" method="post" action="/register">
        <h2>Register</h2>
        <p>Enter your data</p>
    	<input type="text" name="username" placeholder="Enter your name" required>
    	<?php if($error && $error['field'] == 'username'): ?>
        	<div class="error">
    			<?php echo $error['message']; ?>
        	</div>
    	<?php endif; ?>
        
        <input type="email" name="email" placeholder="Enter your email" required>    
    	
        <input type="password" name="password" placeholder="Enter your password" required>
    		
        <button type="submit" name="register">Register</button>
        <div class="clearfix"></div>
    </form>
</div>