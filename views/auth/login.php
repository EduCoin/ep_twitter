<?php 
include 'views/partials/header.php';
?>

<div id="wrapper" class="bg-page login-bg">
	<form id="login" method="post" action="/login">
	    <h2>Login</h2>
	    <p>Enter your data</p>
	    <input type="email" name="email" placeholder="Enter your email" required>    
		
	    <input type="password" name="password" placeholder="Enter your password" required>
			
	    <button type="submit" name="login">Login</button>
	    <div class="clearfix"></div>
	</form>
</div>

<?php 
include 'views/partials/footer.php';
?>


