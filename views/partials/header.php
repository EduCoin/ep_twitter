<!doctype html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<link rel="stylesheet" href="/resources/style.css"/>
	<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.0.0/cropper.min.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.0.0/cropper.min.js"></script>
	<title>Twitter sample</title>
</head>
<body>

<header>
	<div class="container">
		<ul id="nav-menu">
			<li><a href="/home"<?php if($currentPage == "home"): ?> class="active" <?php endif; ?>>Home</a></li>
			<li><a href="/about"<?php if($currentPage == "about"): ?> class="active"<?php endif; ?>>About</a></li>
			<li><a href="/contacts"<?php if($currentPage == "contacts"): ?> class="active"<?php endif; ?>>Contacts</a></li>
		</ul>

		<ul id="nav-auth">
			<?php if(!isset($_SESSION['user'])): ?>
				<li><a href="/login"<?php if($currentPage == "login"): ?> class="active"<?php endif; ?>>login</a></li>
				<li><a href="/register"<?php if($currentPage == "register"): ?> class="active"<?php endif; ?>>register</a></li>
			<?php else: ?>
				<li><a href="/account"<?php if($currentPage == "account"): ?> class="active"<?php endif; ?>>Профиль</a></li>
				<li><a href="/logout">Выход</a></li>
			<?php endif; ?>
		</ul>
		<div class="clearfix"></div>
	</div>
</header>
